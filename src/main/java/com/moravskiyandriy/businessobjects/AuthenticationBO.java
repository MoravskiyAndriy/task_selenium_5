package com.moravskiyandriy.businessobjects;

import com.moravskiyandriy.pageobjects.implementations.AuthenticationPage;

public class AuthenticationBO {
    private AuthenticationPage authenticationPage = new AuthenticationPage();

    public void loginIntoGmail(String login, String password) {
        authenticationPage.goToGmailPage();
        authenticationPage.fillLoginField(login);
        authenticationPage.submitLogin();
        authenticationPage.fillPasswordField(password);
        authenticationPage.submitPassword();
    }
}
