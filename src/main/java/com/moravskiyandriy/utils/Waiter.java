package com.moravskiyandriy.utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    private Waiter() {
    }

    public static WebElement waitForVisibilityOfElement(WebElement element, int time) {
        return (new WebDriverWait(DriverManager.getDriver(), time))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element, int time) {
        return (new WebDriverWait(DriverManager.getDriver(), time))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
