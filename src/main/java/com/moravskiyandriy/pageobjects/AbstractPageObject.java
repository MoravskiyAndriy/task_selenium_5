package com.moravskiyandriy.pageobjects;

import com.moravskiyandriy.utils.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AbstractPageObject {
    protected WebDriver driver;

    protected AbstractPageObject() {
        driver = DriverManager.getDriver();
        PageFactory.initElements(driver, this);
    }
}
