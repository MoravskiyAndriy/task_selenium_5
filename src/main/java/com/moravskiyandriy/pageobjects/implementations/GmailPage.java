package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GmailPage extends AbstractPageObject {
    private static final Logger logger = LogManager.getLogger(GmailPage.class);
    private static final int SMALL_WAIT = 10;
    private static final int MEDIUM_WAIT = 15;
    @FindBy(css = "div.aio.UKr6le")
    private WebElement incomingLettersButton;
    @FindBy(css = "td.oZ-x3.xY")
    private List<WebElement> incomingLettersCheckboxes;
    @FindBy(css = "div.T-I.J-J5-Ji.nX.T-I-ax7.T-I-Js-Gs.mA")
    private WebElement deleteButton;

    public void clickIncomingLettersButton() {
        logger.info("going to Incoming Letters");
        try {
            incomingLettersButton.click();
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
            incomingLettersButton.click();
        }
    }

    public void checkLettersForDeletion(int quantity) {
        logger.info("checking Letters for deletion");
        if (incomingLettersCheckboxes.size() < quantity) {
            logger.warn("Not enough letters");
        } else {
            for (int i = 0; i < quantity; i++) {
                Waiter.waitForElementToBeClickable(incomingLettersCheckboxes.get(i), SMALL_WAIT);
                incomingLettersCheckboxes.get(i).click();
            }
        }
    }

    public void clickDeleteButton() {
        logger.info("clicking Delete Button");
        Waiter.waitForElementToBeClickable(deleteButton, MEDIUM_WAIT);
        deleteButton.click();
    }

    public int countIncomingLetters() {
        logger.info("getting letters' quantity");
        Waiter.waitForElementToBeClickable(incomingLettersButton, MEDIUM_WAIT);
        incomingLettersButton.click();
        return incomingLettersCheckboxes.size();
    }
}
