package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AuthenticationPage extends AbstractPageObject {
    private static final Logger logger = LogManager.getLogger(GmailPage.class);
    private static final int SMALL_WAIT = 10;
    @FindBy(css = "input[type='email']")
    private WebElement loginField;
    @FindBy(css = "input[name='password']")
    private WebElement passwordField;
    @FindBy(id = "identifierNext")
    private WebElement submitLoginButton;
    @FindBy(id = "passwordNext")
    private WebElement submitPasswordButton;

    public void goToGmailPage() {
        logger.info("going to Gmail Page");
        driver.get("https://mail.google.com/mail");
    }

    public void fillLoginField(String login) {
        logger.info("filling Login Field");
        loginField.sendKeys(login);
    }

    public void submitLogin() {
        logger.info("submitting Login");
        Waiter.waitForElementToBeClickable(submitLoginButton, SMALL_WAIT);
        submitLoginButton.click();
    }

    public void fillPasswordField(String password) {
        logger.info("filling Password Field");
        passwordField.sendKeys(password);
    }

    public void submitPassword() {
        logger.info("submitting Password");
        Waiter.waitForElementToBeClickable(submitPasswordButton, SMALL_WAIT);
        submitPasswordButton.click();
    }
}
