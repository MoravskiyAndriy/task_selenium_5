package com.moravskiyandriy.businessobjects;

import com.moravskiyandriy.pageobjects.implementations.GmailPage;
import com.moravskiyandriy.pageobjects.implementations.ReverseActionPopUp;

public class GmailBO {
    private GmailPage gmailPage = new GmailPage();
    private ReverseActionPopUp reverseActionPopUp = new ReverseActionPopUp();

    public int goToIncomingAndCountLetters() {
        gmailPage.clickIncomingLettersButton();
        return gmailPage.countIncomingLetters();
    }

    public void chooseLettersDeleteUndo(int quantity) {
        gmailPage.checkLettersForDeletion(quantity);
        gmailPage.clickDeleteButton();
        reverseActionPopUp.clickUndoButton();
    }
}
