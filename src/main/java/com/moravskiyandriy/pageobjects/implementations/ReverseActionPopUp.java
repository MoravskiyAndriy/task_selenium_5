package com.moravskiyandriy.pageobjects.implementations;

import com.moravskiyandriy.pageobjects.AbstractPageObject;
import com.moravskiyandriy.utils.Waiter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReverseActionPopUp extends AbstractPageObject {
    private static final Logger logger = LogManager.getLogger(GmailPage.class);
    @FindBy(css = "span#link_undo")
    private WebElement undoButton;

    public void clickUndoButton() {
        Waiter.waitForVisibilityOfElement(undoButton, 20);
        Waiter.waitForElementToBeClickable(undoButton, 20);
        logger.info("clicking Undo Button");
        undoButton.click();
    }
}
